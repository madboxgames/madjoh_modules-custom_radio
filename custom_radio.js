define([
	'require',
	'madjoh_modules/controls/controls',
],
function(require, Controls){
	var styles = ['custom_radio'];
	MadJohRequire.getCSS(styles);
	
	var CustomRadio = {
		activeString : 'active',
		activeReg : new RegExp('(\\s|^)'+'active'+'(\\s|$)'),

		init : function(group){
			var element = group.querySelectorAll('input[type="radio"]');
			for(var i=0; i<element.length; i++){
				element[i].addEventListener('change', CustomRadio.select, false);
			}

			CustomRadio.showSelectedRadio(group);
		},
		select : function(e){
			var group = e.target;
			while(group.className.indexOf('custom-radio-group')===-1) group = group.parentNode;

			var active = group.querySelector('.custom-radio-button.'+CustomRadio.activeString);
			if(active){
				active.className = active.className.replace(CustomRadio.activeReg,' ');
				var img = active.querySelector('.custom-radio-img');
				if(img){
					img.src = img.src.substr(0, img.src.length-11)+'.png';
				}
			}

			CustomRadio.showSelectedRadio(group);
		},
		showSelectedRadio : function(group){
			var active = group.querySelector('input[type="radio"]:checked + .custom-radio-button');
			if(active && active.className.indexOf(CustomRadio.activeString)===-1) active.className += ' '+CustomRadio.activeString;
			if(active){
				var img = active.querySelector('.custom-radio-img');
				if(img && img.src.indexOf('active')===-1){
					img.src = img.src.substr(0,img.src.length-4)+'_active.png';
				}
			}
		},
		deselect : function(group){
			var active = group.querySelector('.custom-radio-button.'+CustomRadio.activeString);
			if(active){
				active.className = active.className.replace(CustomRadio.activeReg,' ');
				var img = active.querySelector('.custom-radio-img');
				if(img){
					img.src = img.src.substr(0, img.src.length-11)+'.png';
				}
			}
			group.querySelector('input[type="radio"]:checked').checked = false;
		}
	};

	var element = document.querySelectorAll('.custom-radio-group');
	for(var i=0; i<element.length; i++) CustomRadio.init(element[i]);

	return CustomRadio;
});